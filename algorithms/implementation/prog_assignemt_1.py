#!/usr/bin/env python 
from math import ceil, floor

# working only for unsigned integer
def simple_integer_multiplication(x: str, y: str) -> str:
    rest = 0
    full = []

    for i in range(len(y)-1, -1, -1):
        sum_index = (len(y) - i - 1)
        for j in range(len(x)-1, -1, -1):
            t = int(x[j]) * int(y[i]) + rest
            rest = 0
            if len(full) <= sum_index:
                num = str(t)
                if t >= 10:
                    rest = (t - (t % 10)) // 10
                    num = str(t % 10)
                full.append(num)
            else:
                t += int(full[sum_index])
                num = str(t)
                if t >= 10:
                    rest = (t - (t % 10)) // 10
                    num = str(t % 10)
                full[sum_index] = num
            sum_index+=1

        if rest != 0:
            full.append(str(rest))
        rest = 0
    if rest != 0:
            full.append(str(rest))

    return ''.join(full[::-1])

# https://brilliant.org/wiki/karatsuba-algorithm/
def karatsuba(x: int, y: int):
    #base case
    if x < 10 and y < 10:
        return x*y

    n = max(len(str(x)), len(str(y)))
    m = n//2   #Cast n into a float because n might lie outside the representable range of integers.

    x_H  = x // 10**m
    x_L = x % (10**m)

    y_H = y // 10**m
    y_L = y % (10**m)

    #recursive steps
    a = karatsuba(x_H,y_H)
    d = karatsuba(x_L,y_L)
    e = karatsuba(x_H + x_L, y_H + y_L) - a - d

    return int(a*(10**(m*2)) + e*(10**m) + d)


assert simple_integer_multiplication("0", "0") == "0"
assert simple_integer_multiplication("9", "9") == "81"
assert simple_integer_multiplication("123", "46") == "5658"
assert simple_integer_multiplication("99999", "11111") == "1111088889"
assert simple_integer_multiplication("20974944592", "59574966967627") == "1249581631216206582723184"
assert simple_integer_multiplication("592", "7627") == "4515184"
assert simple_integer_multiplication("3141592653589793238462643383279502884197169399375105820974944592", "2718281828459045235360287471352662497757247093699959574966967627") ==  "8539734222673567065463550869546574495034888535765114961879601127067743044893204848617875072216249073013374895871952806582723184"

print(karatsuba(1234, 4321))
print(karatsuba(123, 1234))