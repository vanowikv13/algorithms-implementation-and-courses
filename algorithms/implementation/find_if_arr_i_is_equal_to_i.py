#!/usr/bin/env python

"""
Problem: You are given a sorted (from smallest to largest) array A of n distinct integers
         which can be positive, negative, or zero.
         You want to decide whether or not there is an index i such that A[i] = i.
         Design the fastest algorithm that you can for solving this problem.

Solution: find_index_equal_to_arr_index_element(arr)
"""


def find_index_equal_to_arr_index_element(arr: list) -> bool:
    return check_if_index(arr, 0, len(arr))


def check_if_index(arr: list, left: int, right: int) -> bool:
    if left >= right:
        return False

    mid = (left+right) // 2
    if arr[mid] == mid:
        return True

    if arr[mid] < 0:
        return check_index(arr, mid+1, right)

    if arr[mid] > mid:
        return (check_index(arr, left, mid) or check_index(arr, arr[mid]+1, right))
    return (check_index(arr, left, arr[mid]-1) or check_index(arr, mid+1, right))


print(find_index_equal_to_arr_index_element([-1, 0, 1, 2, 3, 4, 5, 7]))
print(find_index_equal_to_arr_index_element([0, 2, 3, 4, 5, 6, 7, 12]))
print(find_index_equal_to_arr_index_element([-10, -5, 0, 2, 3, 5, 6, 7]))
print(find_index_equal_to_arr_index_element([-10, -5, 0, 2, 3, 6, 7, 9, 12, 14]))
