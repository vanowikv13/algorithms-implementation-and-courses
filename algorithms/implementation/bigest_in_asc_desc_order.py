#!/usr/bin/env python
"""
Problem: You are a given a unimodal array of n distinct elements,
         meaning that its entries are in increasing order up until its maximum element,
         after which its elements are in decreasing order.
         Give an algorithm to compute the maximum element 

Solution: max_asc_desc(arr) -> int
"""

def find_max(arr, left, right, max) -> int:
    if left >= right:
        return max

    mid  = (left + right) // 2
    if(arr[mid] > max):
        max = arr[mid]

    if arr[mid - 1] > arr[mid]:
        return find_max(arr, left, mid, max)
    return find_max(arr, mid+1, right, max)

def max_asc_desc(arr: list) -> int:
    return find_max(arr, 1, len(arr), arr[0])



print(max_asc_desc([1,2,3,4,5,6,5,4,3,2,1]))
print(max_asc_desc([0,2,4,6,9,6,3,1]))