#!/usr/bin/env python

# O(n^3)
def multiply_matrix(x, y):
    if(len(x) > len(y)):
        t = x
        x = y
        y = t

    s = len(x[0]) if len(x) > len(x[0]) else len(x)
    z = [[0 for j in range(s)] for i in range(s)]
    for i in range(len(x)):
        for l in range(len(y[0])):
            for j in range(len(x[i])):
                z[i][l] += x[i][j] * y[j][l]
    return z

print(multiply_matrix(
[[1, 2, 3],
[4, 5, 6]],
[[1, 4],
 [2, 5],
 [3, 6]]))

print(multiply_matrix([[1]], [[2]])) 