#!/usr/bin/env python

"""
Problem: You are given as input an unsorted array of n distinct numbers,
         where n is a power of 2. Give an algorithm that identifies the second-largest number in the array

Solution: second_largest(arr)
"""

# works only for arrays with distinct elements
def second_largest(arr):
    if(len(arr) == 1):
        return arr[0]
    if(len(arr) == 2):
        return arr[0] if arr[0] < arr[1] else arr[1]

    left_max = find_max(arr[len(arr) // 2:])
    right_max = find_max(arr[:len(arr) // 2])

    if (left_max[0] > right_max[0]):
        if(left_max[1] > right_max[0]):
            return left_max[1]
        return right_max[0]
    if(right_max[1] > left_max[0]):
        return right_max[1]

def find_max(arr):
    if(len(arr) == 1):
        return [arr[0], arr[0]]
    if(len(arr) == 2):
        return [arr[0], arr[1]] if arr[0] > arr[1] else [arr[1], arr[0]]
    
    left_max = find_max(arr[:len(arr)//2])
    right_max = find_max(arr[len(arr)//2:])

    if (left_max[0] < right_max[0]):
        left_max[1] = left_max[0]
        left_max[0] = right_max[0]
        if(left_max[1] < right_max[1]):
            left_max[1] = right_max[1]
    else:
        if(left_max[1] < right_max[0]):
            left_max[1] = right_max[0]
    return left_max


print(second_largest([1,2,3,4,5,6,7,8,9]))
print(second_largest([2,0,3,4,9,1,5,7]))
print(second_largest([1,2]))