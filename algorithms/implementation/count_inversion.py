#!/usr/bin/env python

# divide and conquer version: O(((n^2)/4)*logn) time complexity (with countSplitInv)
def count_inversions(arr):
    if len(arr) <= 1:
        return 0

    middle = len(arr) // 2

    x = count_inversions(arr[:middle])
    y = count_inversions(arr[middle:])
    z = count_split_inv(arr[:middle], arr[middle:])

    return x + y + z

def count_split_inv(left, right):
    count = 0
    i = 0
    j = 0
    while i < len(left):
        if left[i] > right[j]:
            count += 1
            k = j + 1
            while k < len(right):
                if left[i] > right[k]:
                    count += 1
                k+=1
        i+=1
    return count

# it's combination of merge sort and algorithm count_inversions giving us: O(n * logn) which is even faster
# Algorithm return a tuple of (sorted_array, inversion_count)
def count_and_sort_inversions(arr):
    if len(arr) <= 1:
        return (arr, 0)

    middle = len(arr) // 2

    x = count_and_sort_inversions(arr[:middle])
    y = count_and_sort_inversions(arr[middle:])
    z = count_and_merge_split_inv(x[0], y[0])
    return (z[0], z[1] + x[1] + y[1])

def count_and_merge_split_inv(left, right):
    count = 0
    result = []
    left_i, right_i = 0, 0 
    while left_i < len(left) and right_i < len(right):
        if left[left_i] <= right[right_i]:
            result.append(left[left_i])
            left_i += 1
        else:
            result.append(right[right_i])
            right_i += 1
            count += len(left) - left_i
    if left:
        result.extend(left[left_i:])
    if right:
        result.extend(right[right_i:])
    return (result, count)

print(count_inversions([1,3,5,2,4,6])) # 3
print(count_inversions([1,2,3,4,5,6])) # 0
print(count_inversions([6,5,4,3,2,1])) # 15

print(count_and_sort_inversions([1,3,5,2,4,6])) # 3
print(count_and_sort_inversions([1,2,3,4,5,6])) # 0
print(count_and_sort_inversions([6,5,4,3,2,1])) # 15