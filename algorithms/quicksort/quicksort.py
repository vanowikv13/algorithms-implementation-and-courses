#!/usr/bin/env python
import random
import time

comp = 0

#------------Version with last element as pivot-------------------------
# O(n)
def partition_last(arr, left, right):
    i = left
    pivot = arr[right]
 
    for j in range(left, right):
        #global comp
        #comp += 1
        if arr[j] <= pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i+1
 
    arr[i], arr[right] = arr[right], arr[i]
    return i
 
 # average case: O(n log n), worst case: O(n^2)
def quicksort_last(arr, left, right):
    if len(arr) == 1:
        return arr
    if left < right:
        p_index = partition_last(arr, left, right)
        quicksort_last(arr, left, p_index-1)
        quicksort_last(arr, p_index+1, right)
    return arr
#-------------------------------------------------------------------

#----------------Second version with first element as Pivot-----------------
# O(n)
def partition_first(arr, left, right):
    i = left+1
    pivot = arr[left]
 
    for j in range(left+1, right+1):
        #global comp
        #comp += 1
        if arr[j] <= pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i+1
 
    arr[i-1], arr[left] = arr[left], arr[i-1]
    return i-1
 
 # average case: O(n log n), worst case: O(n^2)
def quicksort_first(arr, left, right):
    if right <= 1:
        return arr
    if left < right:
        p_index = partition_first(arr, left, right)
        quicksort_first(arr, left, p_index-1)
        quicksort_first(arr, p_index+1, right)
    return arr
#----------------------------------------------------------------------------

#----- Third version with random pivot----------------------------------------
# O(n)
def partition_random(arr, left, right):
    rand_i = random.randint(left, right)
    pivot = arr[rand_i]
    arr[right], arr[rand_i] = arr[rand_i], arr[right]
    i = left
 
    for j in range(i, right):
        #global comp
        #comp += 1
        if arr[j] <= pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i+1

    arr[i], arr[right] = arr[right], arr[i]
    return i
 
 # average case: O(n log n), worst case: O(n^2)
def quicksort_random(arr, left, right):
    if right <= 1:
        return arr
    if left < right:
        p_index = partition_random(arr, left, right)
        quicksort_random(arr, left, p_index-1)
        quicksort_random(arr, p_index+1, right)
    return arr

#----- Fourth version with median pivot of first, middle, last----------------------------------------
# O(n)
def partition(arr, left, right):
    mid = (left+right)//2

    #replacing median with last element
    if(arr[left] > arr[mid]):
        if(arr[mid] > arr[right]):
            arr[right], arr[mid] = arr[mid], arr[right]
        elif(arr[right] > arr[left]):
            arr[right], arr[left] = arr[left], arr[right]
    elif(arr[right] > arr[mid]):
        arr[right], arr[mid] = arr[mid], arr[right]
    elif(arr[left] > arr[right]):
        arr[right], arr[left] = arr[left], arr[right]

    pivot = arr[right]
    i = left
    for j in range(i, right):
        #global comp
        #comp += 1
        if arr[j] <= pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i+1

    arr[i], arr[right] = arr[right], arr[i]
    return i
 
 # average case: O(n log n), worst case: O(n^2)
def quicksort(arr, left, right):
    if right <= 1:
        return arr
    if left < right:
        p_index = partition(arr, left, right)
        quicksort(arr, left, p_index-1)
        quicksort(arr, p_index+1, right)
    return arr

#-----------------------------------------------------------------------------


"""
print(quicksort_first([6,5,4,3,1, 2], 0, 5))
print(quicksort([2, 3, 1, 5, 6, 4], 0, 5))
print(quicksort([6,5,3,2,4,3,2,2,2,1,1,1], 0, 11))
print(quicksort([6,5,4,3,2,1], 0, 5))
"""

with open("QuickSort.txt", "r") as test:
    l = test.readlines()
    n = len(l)-1
    arr = []
    for x in l:
        arr.append(int(x[:-1]))

    #start = time.time()
    #quicksort_last(arr, 0, n)
    #end = time.time()
    #print(comp) #160361
    #print(end - start) #0.026621103286743164

    #start = time.time()
    #quicksort_first(arr, 0, n)
    #end = time.time()
    #print(comp) #162085
    #print(end - start) #0.027529239654541016

    #start = time.time()
    #quicksort_random(arr, 0, n)
    #end = time.time()
    #print(comp) #144416 - 164763 (mostly around 152000)
    #print(end - start) #0.029938459396362305 avg

    #start = time.time()
    #quicksort(arr, 0, n)
    #end = time.time()
    #print(comp) #133868
    #print(end - start) #0.023857593536376953