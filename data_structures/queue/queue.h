#pragma once
#include <stdexcept>
#include <string>

/*implementation of basic functionality Queue (FIFO) structure using one direction linked list
    Linked list was chose instead of array cause of performance reasons, because calling a dequeue
    in linked list take O(1) time, while using array or dynamic array takes O(n) time.
    (cause after we dequeue we need to move all elements backward)
    The rest of operation take simillar time in both of operations.
*/
template<class T>
class Queue {
protected:
    struct Node {
        T value;
        Node* next;

        Node(T val, Node* n = nullptr) : value(val), next(n) {}
    };

    Node* head = nullptr, *tail = nullptr;
    size_t _size=0;
public:
    //O(1)
    void enqueue(T value) {
        Node* tmp = new Node(value);
        if(_size == 0) {
            head = tmp;
            tail = tmp;
        } else {
            tmp->next = nullptr;
            tail->next = tmp;
            tail = tmp;
        }
        _size++;
    }

    //O(1)
    T dequeue() {
        if(_size == 0)
            throw std::string("Queue is empty. Could not dequeue.");
        
        T ret = head->value;
        if(_size == 1) {
            tail = nullptr;
            delete head;
            head = nullptr;
        } else {
            Node* tmp = head->next;
            delete head;
            head = tmp;
        }
        _size--;
        return ret;
    }
    
    size_t size() const {
        return _size;
    }

    //O(1)
    T& peek() const {
        if(_size == 0)
            throw std::string("Queue is empty. Could not peek.");

        return head->value;
    }
};