#include <iostream>
#include "queue.h"
#include <string>

int main() {
    Queue<int> queue;
    for(size_t i = 0; i < 3; i++) {
        queue.enqueue(i+1);
    }
    
    std::cout << queue.size() << std::endl;
    
    std::cout << queue.peek() << std::endl;

    try {
        for (size_t i = 0; i < 4; i++)
        {
            std::cout << queue.dequeue() << ",";
        }
        std::cout << std::endl;
        
    } catch(std::string err) {
        std::cerr << std::endl << err << std::endl;
    }
}
