#include <iostream>
#include <list>
#include <random>
#include <algorithm>
#include <vector>

#include "red_black_tree.h"

int main(int argc, char const *argv[])
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(1,10);
    RedBlackTree<int> rbt;

    std::vector<int> values;
    for (int i = 0; i < 10; i++) {
        int tmp = dist6(rng);
        while (find(values.begin(), values.end(), tmp) != values.end())
            tmp = dist6(rng);
        rbt.insert(tmp, rbt.root());
        values.push_back(tmp);
    }

    std::list<int> tl = rbt.inorder(*rbt.root());
    std::vector<int> x(tl.begin(), tl.end());
    for(auto &xi : x)
        std::cout << xi << std::endl;

    std::cout << "Size: " << rbt.size() << std::endl;
    std::shuffle(x.begin(), x.end(), std::mt19937(std::random_device()()));
    for(auto & xi : x)
      rbt.remove(xi, rbt.root());
    std::cout << "Size: " << rbt.size() << std::endl;
    return 0;
}