#pragma once
#include <list>
#include <string>
#include <vector>
#include <queue>
#include <stack>

/*
    Rules of Red-Black-Tree
    1. Each node is either red or black.
    2. The root is black. This rule is sometimes omitted. Since the root can always be changed from red to black, but not necessarily vice versa, this rule has little effect on analysis.
    3. All leaves (nullptr) are black.
    4. If a node is red, then both its children are black.
    5. Every path from a given node to any of its descendant NIL nodes goes through the same number of black nodes.
 */

template <class T>
class RedBlackTree
{
    enum class Color : bool {
        RED,
        BLACK
    };
    
    class Node
    {
    public:
        Color color;
        T value;
        Node *left;
        Node *right;
        Node *parent;
        explicit Node(T val, Color c=Color::RED, Node* p= nullptr, Node*l= nullptr, Node* r= nullptr) : value(val), color(c), parent(p), left(l), right(r) {}
    };

    size_t size_ = 0;
    Node *root_ = nullptr;

public:
    RedBlackTree() = default;

    std::list<T> preorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values{node->value};
        if (node->left != nullptr)
            values.splice(values.end(), preorder(node->left));

        if (node->right != nullptr)
            values.splice(values.end(), preorder(node->right));

        return values;
    }

    std::list<T> inorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values;
        if (node->left != nullptr)
            values.splice(values.end(), inorder(node->left));

        values.push_back(node->value);
        if (node->right != nullptr)
            values.splice(values.end(), inorder(node->right));
        return values;
    }

    Node* firstBfs(Node *node, T value) {
        if (node == nullptr)
            return nullptr;
        std::vector<Node*> nodes{node};
        std::vector<Node*> nextNodes;

        while(nodes.size() != 0) {
            for(auto &x : nodes) {
                if(x->value == value)
                    return x;
                if(x->left != nullptr)
                    nextNodes.push_back(x->left);
                if(x->right != nullptr)
                    nextNodes.push_back(x->right);
            }
            nodes = std::move(nextNodes);
        }
        return nullptr;
    }

    //it's not optimal algorithm for finding node in tree
    //it's better to use find to find a node value
    //it's also return the way we search a tree untill we found an element.
    std::pair<Node*, std::vector<T>> bfs(Node *node, T value) {
        std::queue<Node*> q;
        q.push(node);
        std::vector<T> values;
        while(!q.empty()) {
            Node* v = q.front();
            values.push_back(node->value);
            q.pop();
            if(v->value == value)
                return std::pair<Node*, std::vector<T>>(v, values);
            if(v->left != nullptr)
                q.push(v->left);
            if(v->right != nullptr)
                q.push(v->right);
        }
        return std::pair<Node*, std::vector<T>>(nullptr, values);
    }

    std::pair<Node*, std::vector<T>> dfs(Node *node, T value) {
        std::stack<Node*> s;
        s.push(node);
        std::vector<T> values;
        while(!s.empty()) {
            Node* v = s.top();
            values.push_back(node->value);
            s.pop();
            if(v->value == value)
                return std::pair<Node*, std::vector<T>>(v, values);
            if(v->left != nullptr)
                s.push(v->left);
            if(v->right != nullptr)
                s.push(v->right);
        }
        return std::pair<Node*, std::vector<T>>(nullptr, values);
    }


    void rotateLeft(Node* node) {
        if(node != nullptr &&  node->right != nullptr) {
            if(node->parent != nullptr) {
                if(node->parent->right == node) {
                    node->parent->right = node->right;
                } else {
                    node->parent->left = node->right;
                }
                node->right->parent = node->parent;
            }
            Node* tmp = node->right;
            if(tmp->left != nullptr)
                tmp->left->parent = node;
            node->right = node->right->left;
            tmp->left = node;
            tmp->parent = node->parent;
            node->parent = tmp;

            if(node == root_)
                root_ = tmp;
        }
    }

    void rotateRight(Node* node) {
        if(node != nullptr && node->left != nullptr) {
            if(node->parent != nullptr) {
                if(node->parent->right == node) {
                    node->parent->right = node->left;
                } else {
                    node->parent->left = node->left;
                }
                node->left->parent = node->parent;
            }
            Node* tmp = node->left;
            if(tmp->right != nullptr)
                tmp->right->parent = node;
            node->left = node->left->right;
            tmp->right = node;
            tmp->parent = node->parent;
            node->parent = tmp;

            if(node == root_)
                root_ = tmp;
        }
    }

    void insert(T value, Node **node)
    {
        if (node == nullptr || *node == nullptr)
        {
            *node = new Node(value, Color::BLACK);
            size_++;
        }
        else if (value < (*node)->value)
        {
            if ((*node)->left == nullptr)
            {
                (*node)->left = new Node(value, Color::RED, (*node));
                size_++;
                insertFix((*node), (*node)->left);
            }
            else
                insert(value, &((*node)->left));
        }
        else
        {
            if ((*node)->right == nullptr)
            {
                (*node)->right = new Node(value, Color::RED, (*node));
                size_++;
                insertFix((*node), (*node)->right);
            }
            else
                insert(value, &((*node)->right));
        }
    }

    void remove(T value, Node **node)
    {
        if (*node == nullptr)
            return;
        else if (value < (*node)->value)
            remove(value, &((*node)->left));
        else if (value > (*node)->value)
            remove(value, &((*node)->right));
        else
        {
            Node* parent = (*node)->parent;
            //deleting the leaf
            if ((*node)->left == nullptr && (*node)->right == nullptr)
            {
                if((*node) == root_) {
                    delete root_;
                    root_ = nullptr;
                }
                else if (parent->left == (*node))
                {
                    delete parent->left;
                    parent->left = nullptr;
                    //checking siblings
                    if(parent->right != nullptr) {
                        if(parent->right->left != nullptr || parent->right->right != nullptr) {
                            rotateLeft(parent);
                            setChildrenToColor(parent->parent, parent->parent->color);

                            if(parent->parent == root_)
                                parent->parent->color = Color::BLACK;
                            else
                                parent->parent->color = getOppositeColor(parent->parent->color);

                        }
                    }
                }
                else
                {
                    delete parent->right;
                    parent->right = nullptr;

                    if(parent->left != nullptr) {
                        if(parent->left->left != nullptr || parent->left->right != nullptr) {
                            rotateRight(parent);
                            setChildrenToColor(parent->parent, parent->parent->color);
                            if(parent->parent == root_)
                                parent->parent->color = Color::BLACK;
                            else
                                parent->parent->color = getOppositeColor(parent->parent->color);
                        }
                    }
                }
                size_--;
                return;
            }
            Node *directionTmp = (*node)->left == nullptr ? (*node)->right : (*node)->left;
            if((*node)->left == nullptr || (*node)->right == nullptr)
            {
                Node *tmp = (*node);
                if(parent == nullptr) {
                    root_ = directionTmp;
                    root_->color = Color::BLACK;
                }
                else if(parent->right == (*node)) {
                    parent->right = directionTmp;
                    directionTmp->color = (*node)->color;
                }
                else {
                    parent->left = directionTmp;
                    directionTmp->color = (*node)->color;
                }
                directionTmp->parent = parent;
                delete tmp;
                size_--;
            }
            else
            {
                Node *successor = (*node)->left;
                while (successor->right != nullptr)
                    successor = successor->right;
                (*node)->value = successor->value;
                remove(successor->value, &successor);
            }
        }
    }

    Node* find(T value, Node*node) {
        if(node == nullptr)
            return nullptr;
        if(node->value == value)
            return node;
        if(node->value < value)
            return find(value, node->left);
        if(node->value > value)
            return find(value, node->right);
        return nullptr;
    }

    uint height(Node* node) {
        if(node == nullptr)
            return 0;
        uint left = height(node->left);
        uint right = height(node->right);
        if(left > right)
            return left + 1;
        return right + 1;
    }

    void clear() {
        while(root_ != nullptr)
            remove(root_->value, &root_);
    }

    Node **root()
    {
        return &root_;
    }

    size_t size()
    {
        return size_;
    }

private:
    Node* findMax(Node* node) {
        if(node != nullptr && node->right != nullptr)
            return findMax(node->right);
        return node;
    }

    Color getOppositeColor(Color color) {
        return color == Color::RED ? Color::BLACK : Color::RED;
    }

    void setChildrenToColor(Node* node, Color color) {
        if(node->left != nullptr)
            node->left->color = color;
        if(node->right != nullptr)
            node->right->color = color;
    }

    Node* getSibling(Node* node) {
        if(node->parent != nullptr) {
            if(node->parent->left == node)
                return node->parent->right;
            return node->parent->left;
        }
        return nullptr;
    }

    void setColorWithCheck(Node* node, Color color) {
        if(node != nullptr)
            node->color = color;
    }

    void fixColorsAfterRotation(Node* node, bool left=true) {
        node->color = Color::BLACK;
        if(node->parent != root_)
            setColorWithCheck(node->parent, Color::RED);
        if(left)
            setColorWithCheck(node->left, Color::RED);
        else
            setColorWithCheck(node->right, Color::RED);
    }

    void insertFix(Node* node, Node* child) {
        if(node != nullptr && node->color != Color::BLACK) {
            Node* sibling = getSibling(node);
            if (sibling != nullptr && sibling->color == Color::RED) {
                setChildrenToColor(node->parent, Color::BLACK);
                if(node->parent != root_) {
                    node->parent->color = Color::RED;
                    insertFix(node->parent->parent, node->parent);
                }
            }
            else {
                //right part of parent
                if (node->parent->value < node->value) {
                    if(child->value < node->value) {
                        rotateRight(node);
                        rotateLeft(child->parent);
                        fixColorsAfterRotation(child);
                    } else {
                        rotateLeft(node->parent);
                        fixColorsAfterRotation(node);
                    }
                } //left part of parent
                else {
                    if(child->value > node->value) {
                        rotateLeft(node);
                        rotateRight(child->parent);
                        fixColorsAfterRotation(child, false);
                    }else {
                        rotateRight(node->parent);
                        fixColorsAfterRotation(node, false);
                    }
                }
            }
        }
    }

};