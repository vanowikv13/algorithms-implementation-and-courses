#pragma once
#include <string>
#include <time.h>
#include <iostream>
#include <vector>

const size_t NEXT_NODES_CAPACITY = 10;

struct Node
{
    int key;
    double dbl;
    char chr;
    Node* nextNodes[NEXT_NODES_CAPACITY];
    size_t nextNodesSize=0;

    Node(int k, double d = 0, char c = 'T') : key(k), dbl(d), chr(c) {}
};

class LinkedListMap
{
private:
    bool wasCheck = false;
    int keyMinInRand = 99;
    int keyMaxInRand = 99999;
public:
    Node *head = nullptr, *tail = nullptr;
    size_t lSize = 0;

public:
    LinkedListMap() {}

    void add(int key)
    {
        if (lSize == 0)
        {
            head = new Node(key, fRand(0.0, 9999999.0), 'T');
            head->nextNodes[0] = nullptr;
            tail = head;
            lSize++;
        }
        else
        {   
            if(!wasCheck) {
                Node* t = findNode(key);
                if (findNode(key) != nullptr) {
                    t->chr = 'D';
                    return;
                }
                wasCheck = false;
            }
            Node *tmp = new Node(key, fRand(0.0, 9999999.0), 'T');
            std::vector<Node*> previous;
            bool insert = false;

            for (Node *it = head; it != tail->nextNodes[0]; it = it->nextNodes[0])
            {
                if (it->key > tmp->key)
                {
                    insert = true;
                    //means first element
                    if (previous.size() == 0)
                    {
                        tmp->nextNodes[0] = head;
                        for(size_t i = 0; i < head->nextNodesSize && i < 9; i++)
                            tmp->nextNodes[i+1] = head->nextNodes[i];
                        
                        tmp->nextNodesSize = head->nextNodesSize;
                        if(tmp->nextNodesSize < 10)
                            tmp->nextNodesSize++;
                        head = tmp;
                    }
                    //means somewhere in the middle
                    else
                    {
                        tmp->nextNodesSize = previous.back()->nextNodesSize;
                        for(int i = 0; i < previous.back()->nextNodesSize; i++)
                            tmp->nextNodes[i] = previous.back()->nextNodes[i];

                        int j = 0;
                        for(std::vector<Node*>::reverse_iterator i = previous.rbegin(); i != previous.rend() && j < 10; i++, j++)
                            insertNewNodeToNodesArray((*i), tmp, j);
                    }
                    lSize++;
                    return;
                }
                previous.push_back(it);
            }
            //last element
            if (!insert)
            {
                int j = 0;
                for(std::vector<Node*>::reverse_iterator i = previous.rbegin(); i != previous.rend() && j < 10; i++, j++)
                    insertNewNodeToNodesArray((*i), tmp, j);
                tail = tmp;
                tail->nextNodes[0] = nullptr;
            }
            lSize++;
        }
    }
    void addRandomMultiple(size_t n)
    {
        for (size_t i = 0; i < n && lSize < (keyMaxInRand - keyMinInRand + 1); i++)
        {
            int key = 0;
            do {
                key = kRand(keyMinInRand, keyMaxInRand);
            } while(nullptr != findNode(key));

            wasCheck = true;
            add(key);
        }
        wasCheck = false;
    }

    Node *findNode(int key)
    {
        if (lSize == 0)
            return nullptr;
        
        Node * left = head;
        while(left != nullptr) {
            if(left->key == key)
                return left;

            if(left->nextNodesSize != 0 && left->nextNodes[left->nextNodesSize-1]->key < key)
              left = left->nextNodes[left->nextNodesSize-1];
            left = left->nextNodes[0];
        }
        return nullptr;
    }

    void deleteNode(int key)
    {        
        if(lSize == 0) {
            std::cerr << "ERROR: Collection doesn't have elements" << std::endl;
            return;
        }

        std::vector<Node*> previous;
        Node*tmp = nullptr;
        for (Node *it = head; it != tail->nextNodes[0]; it = it->nextNodes[0]) {
            if (it->key == key) {
                tmp = it;
                break;
            }
            previous.push_back(it);
        }

        if(tmp == nullptr) {
            std::cerr << "ERROR: Element with "+std::to_string(key)+" key do not exist." << std::endl;
            return;
        }

        if(previous.size() == 0) {
            head = head->nextNodes[0];
            delete tmp;
        } else if(tail == tmp) {
            size_t j = 0;
            for(std::vector<Node*>::reverse_iterator i = previous.rbegin(); i != previous.rend() && j < 10; i++, j++)
                removeNodeFromNodesArray((*i), tmp, j);
            delete tail;
            tail = previous.back();
        } else {
            size_t j = 0;
            for(std::vector<Node*>::reverse_iterator i = previous.rbegin(); i != previous.rend() && j < 10; i++, j++)
                removeNodeFromNodesArray((*i), tmp, j, tmp->nextNodes[tmp->nextNodesSize-j-1]);
            delete tmp;
        }
        lSize--;
    }

    void printFirst(size_t first_few) {
        if(lSize == 0) {
            std::cerr << "ERROR: Collection doesn't have elements" << std::endl;
            return;
        }

        std::cout << "From begin " << (first_few > lSize ? lSize : first_few) << ": " << std::endl;
        Node* it = head;
        for (size_t i = 0; i < first_few && it != tail->nextNodes[0]; i++, it = it->nextNodes[0])
            std::cout << "key: " << it->key << ", double: " << it->dbl << ", chr: " << it->chr << std::endl;
        std::cout << std::endl;
    }

    void printLast(size_t last_few) {
        if(lSize == 0) {
            std::cerr << "ERROR: Collection doesn't have elements" << std::endl;
            return;
        }

        if(lSize > last_few)
            last_few = lSize - last_few;

        std::vector<Node*> vnds;
        Node* it = head;
        for (size_t i = 0; i < last_few && it != tail->nextNodes[0]; i++, it = it->nextNodes[0]) {}
        
        for (size_t i = 0; i < last_few && it != tail->nextNodes[0]; i++, it = it->nextNodes[0])
            vnds.push_back(it);

        std::cout << "From end " << (last_few > lSize ? lSize : last_few) << ": " << std::endl;
        for (auto it = vnds.rbegin(); it != vnds.rend(); it++)
            std::cout << "key: " << (*it)->key << ", double: " << (*it)->dbl << ", chr: " << (*it)->chr << std::endl;
        std::cout << std::endl;
    }

    size_t size() { return lSize; }

    void clear() {
        for (Node *tmp = head; tmp != nullptr;) {
            Node * next = tmp->nextNodes[0];
            delete tmp;
            tmp = next;
        }
        lSize = 0;
    }

protected:
    double fRand(double fMin, double fMax)
    {
        double f = (double)rand() / RAND_MAX;
        return fMin + f * (fMax - fMin);
    }
    int kRand(int min, int max)
    {
        return (rand() % (max-min) + min);
    }

    void insertNewNodeToNodesArray(Node* mainNode, Node* nodeToInsert, size_t index) {
        if(index >= NEXT_NODES_CAPACITY || index < 0) {
            std::cerr << "Too large or too small index for skip list" << std::endl;
            exit(1);
            return;
        }

        for(size_t i = index+1; i < NEXT_NODES_CAPACITY && i < mainNode->nextNodesSize+1; i++)
            mainNode->nextNodes[i] = mainNode->nextNodes[i-1];
        mainNode->nextNodes[index] = nodeToInsert;
        if(mainNode->nextNodesSize < 10)
            mainNode->nextNodesSize++;
    }


    void removeNodeFromNodesArray(Node* mainNode, Node* nodeToDelete, size_t index, Node* append=nullptr) {
        if(index >= NEXT_NODES_CAPACITY || index < 0) {
            std::cerr << "Too large or too small index for skip list" << std::endl;
            exit(1);
            return;
        }

        for(size_t i = index+1; i < NEXT_NODES_CAPACITY && i < mainNode->nextNodesSize+1; i++)
            mainNode->nextNodes[i-1] = mainNode->nextNodes[i];
        mainNode->nextNodes[mainNode->nextNodesSize-1] = append;
        if(mainNode->nextNodesSize > 0 && append != nullptr)
            mainNode->nextNodesSize--;
    }
};