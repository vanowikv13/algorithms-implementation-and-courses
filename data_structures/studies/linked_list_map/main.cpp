#include <iostream>
#include <time.h>
#include <fstream>

#include "linked_list_map.h"

using namespace std;

int main() {
    srand (time(NULL));

    int X, k1, k2, k3, k4, k5;

    fstream file;
    file.open("inlab03.txt", ios::in);
    if(!file.is_open()) {
        cerr << "File is not specified" << endl;
        return 1;
    }

    file >> X >> k1 >> k2 >> k3 >> k4 >> k5;
    
    file.close();
    clock_t t1 = clock();
    LinkedListMap llm;
    //if key is not found return nullptr
    if(llm.findNode(k1) == nullptr)
        cout << "key is not found" << endl;
    llm.addRandomMultiple(X);
    cout << "Size: " << llm.size() << endl;
    llm.printFirst(20);
    llm.add(k2);
    llm.printFirst(20);
    llm.add(k3);
    llm.printFirst(20);
    llm.add(k4);
    llm.printFirst(20);
    llm.add(k5);
    cout << "Size: " << llm.size() << endl;
    llm.printFirst(20);
    llm.deleteNode(k3);
    llm.deleteNode(k2);
    llm.deleteNode(k5);
    cout << "Size: " << llm.size() << endl;
    llm.printFirst(20);
    llm.clear(); 
    clock_t t2 = clock();
    double full_time = (double)(t2 - t1) / CLOCKS_PER_SEC;
    cout << "Full time: " << full_time << endl;
    return 0;
}