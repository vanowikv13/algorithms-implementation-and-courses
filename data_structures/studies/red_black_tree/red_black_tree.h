#pragma once
#include <list>
#include <string>

template <class T>
class RedBlackTree
{
    enum class Color : bool {
        RED,
        BLACK
    };
    
    class Node
    {
    public:
        Color color;
        T value;
        uint key;
        Node *left;
        Node *right;
        Node *parent;
        explicit Node(T val, uint k, Color c=Color::RED, Node* p= nullptr, Node*l= nullptr, Node* r= nullptr) : value(val), color(c), key(k), parent(p), left(l), right(r) {}

        std::string str(std::string prev) {
            std::string i = (color == Color::RED ? "[Red, " : "[Black, ");
            if(parent != nullptr)
                i += " p: " + std::to_string(parent->key)+",";
            else
                i += " p: NULL,";
            if(left != nullptr)
                i += " l: " + std::to_string(left->key)+",";
            else
                i += " l: NULL,";

            if(right != nullptr)
                i += " r: " + std::to_string(right->key)+" ";
            else
                i += " r: NULL ";

            i+= "]";
            return ("|" + prev + "> key= " + std::to_string(key) + ", value= " + std::to_string(value) + ", " + i + "\n");
        }
    };

    size_t keys = 0;
    size_t size_ = 0;
    Node *root_ = nullptr;

public:
    RedBlackTree() = default;

    std::list<T> preorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values{node->value};
        if (node->left != nullptr)
            values.splice(values.end(), preorder(node->left));

        if (node->right != nullptr)
            values.splice(values.end(), preorder(node->right));

        return values;
    }

    std::list<T> inorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values;
        if (node->left != nullptr)
            values.splice(values.end(), inorder(node->left));

        values.push_back(node->value);
        if (node->right != nullptr)
            values.splice(values.end(), inorder(node->right));
        return values;
    }

    void rotateLeft(Node* node) {
        if(node != nullptr &&  node->right != nullptr) {
            if(node->parent != nullptr) {
                if(node->parent->right == node) {
                    node->parent->right = node->right;
                } else {
                    node->parent->left = node->right;
                }
                node->right->parent = node->parent;
            }
            Node* tmp = node->right;
            if(tmp->left != nullptr)
                tmp->left->parent = node;
            node->right = node->right->left;
            tmp->left = node;
            tmp->parent = node->parent;
            node->parent = tmp;

            if(node == root_)
                root_ = tmp;
        }
    }

    void rotateRight(Node* node) {
        if(node != nullptr && node->left != nullptr) {
            if(node->parent != nullptr) {
                if(node->parent->right == node) {
                    node->parent->right = node->left;
                } else {
                    node->parent->left = node->left;
                }
                node->left->parent = node->parent;
            }
            Node* tmp = node->left;
            if(tmp->right != nullptr)
                tmp->right->parent = node;
            node->left = node->left->right;
            tmp->right = node;
            tmp->parent = node->parent;
            node->parent = tmp;

            if(node == root_)
                root_ = tmp;
        }
    }

    void insert(T value, Node **node)
    {
        if (node == nullptr || *node == nullptr)
        {
            *node = new Node(value, keys++, Color::BLACK);
            size_++;
        }
        else if (value < (*node)->value)
        {
            if ((*node)->left == nullptr)
            {
                (*node)->left = new Node(value, keys++, Color::RED, (*node));
                size_++;
                insertFix((*node), (*node)->left);
            }
            else
                insert(value, &((*node)->left));
        }
        else
        {
            if ((*node)->right == nullptr)
            {
                (*node)->right = new Node(value, keys++, Color::RED, (*node));
                size_++;
                insertFix((*node), (*node)->right);
            }
            else
                insert(value, &((*node)->right));
        }
    }

    void remove(T value, Node **node)
    {
        if (*node == nullptr)
            return;
        else if (value < (*node)->value)
            remove(value, &((*node)->left));
        else if (value > (*node)->value)
            remove(value, &((*node)->right));
        else
        {
            Node* parent = (*node)->parent;
            //deleting the leaf
            if ((*node)->left == nullptr && (*node)->right == nullptr)
            {
                if((*node) == root_) {
                    delete root_;
                    root_ = nullptr;
                }
                else if (parent->left == (*node))
                {
                    delete parent->left;
                    parent->left = nullptr;
                    //checking siblings
                    if(parent->right != nullptr) {
                        if(parent->right->left != nullptr || parent->right->right != nullptr) {
                            rotateLeft(parent);
                            setChildrenToColor(parent->parent, parent->parent->color);

                            if(parent->parent == root_)
                                parent->parent->color = Color::BLACK;
                            else
                                parent->parent->color = getOppositeColor(parent->parent->color);

                        }
                    }
                }
                else
                {
                    delete parent->right;
                    parent->right = nullptr;

                    if(parent->left != nullptr) {
                        if(parent->left->left != nullptr || parent->left->right != nullptr) {
                            rotateRight(parent);
                            setChildrenToColor(parent->parent, parent->parent->color);
                            if(parent->parent == root_)
                                parent->parent->color = Color::BLACK;
                            else
                                parent->parent->color = getOppositeColor(parent->parent->color);
                        }
                    }
                }
                size_--;
                return;
            }
            Node *directionTmp = (*node)->left == nullptr ? (*node)->right : (*node)->left;
            if((*node)->left == nullptr || (*node)->right == nullptr)
            {
                Node *tmp = (*node);
                if(parent == nullptr) {
                    root_ = directionTmp;
                    root_->color = Color::BLACK;
                }
                else if(parent->right == (*node)) {
                    parent->right = directionTmp;
                    directionTmp->color = (*node)->color;
                }
                else {
                    parent->left = directionTmp;
                    directionTmp->color = (*node)->color;
                }
                directionTmp->parent = parent;
                delete tmp;
                size_--;
            }
            else
            {
                Node *successor = (*node)->left;
                while (successor->right != nullptr)
                    successor = successor->right;
                (*node)->value = successor->value;
                remove(successor->value, &successor);
            }
        }
    }

    Node* find(T value, Node*node) {
        if(node == nullptr)
            return nullptr;
        if(node->value == value)
            return node;
        if(node->value < value)
            return find(value, node->left);
        if(node->value > value)
            return find(value, node->right);
        return nullptr;
    }

    uint height(Node* node) {
        if(node == nullptr)
            return 0;
        uint left = height(node->left);
        uint right = height(node->right);
        if(left > right)
            return left + 1;
        return right + 1;
    }

    std::list<std::string> nodeStr(Node*node, std::string prev="-") {
        if(node == nullptr || size_ == 0)
            return std::list<std::string>();

        std::list<std::string> strs{node->str(prev)};
        strs.splice(strs.end(), nodeStr(node->left, prev+"-"));
        strs.splice(strs.end(), nodeStr(node->right, prev+"-"));
        return strs;
    }

    void clear() {
        while(root_ != nullptr)
            remove(root_->value, &root_);
    }

    Node **root()
    {
        return &root_;
    }

    size_t size()
    {
        return size_;
    }

private:
    Node* findMax(Node* node) {
        if(node != nullptr && node->right != nullptr)
            return findMax(node->right);
        return node;
    }

    Color getOppositeColor(Color color) {
        return color == Color::RED ? Color::BLACK : Color::RED;
    }

    void setChildrenToColor(Node* node, Color color) {
        if(node->left != nullptr)
            node->left->color = color;
        if(node->right != nullptr)
            node->right->color = color;
    }

    Node* getSibling(Node* node) {
        if(node->parent != nullptr) {
            if(node->parent->left == node)
                return node->parent->right;
            return node->parent->left;
        }
        return nullptr;
    }

    void setColorWithCheck(Node* node, Color color) {
        if(node != nullptr)
            node->color = color;
    }

    void fixColorsAfterRotation(Node* node, bool left=true) {
        node->color = Color::BLACK;
        if(node->parent != root_)
            setColorWithCheck(node->parent, Color::RED);
        if(left)
            setColorWithCheck(node->left, Color::RED);
        else
            setColorWithCheck(node->right, Color::RED);
    }

    void insertFix(Node* node, Node* child) {
        if(node != nullptr && node->color != Color::BLACK) {
            Node* sibling = getSibling(node);
            if (sibling != nullptr && sibling->color == Color::RED) {
                setChildrenToColor(node->parent, Color::BLACK);
                if(node->parent != root_) {
                    node->parent->color = Color::RED;
                    insertFix(node->parent->parent, node->parent);
                }
            }
            else {
                //right part of parent
                if (node->parent->value < node->value) {
                    if(child->value < node->value) {
                        rotateRight(node);
                        rotateLeft(child->parent);
                        fixColorsAfterRotation(child);
                    } else {
                        rotateLeft(node->parent);
                        fixColorsAfterRotation(node);
                    }
                } //left part of parent
                else {
                    if(child->value > node->value) {
                        rotateLeft(node);
                        rotateRight(child->parent);
                        fixColorsAfterRotation(child, false);
                    }else {
                        rotateRight(node->parent);
                        fixColorsAfterRotation(node, false);
                    }
                }
            }
        }
    }
};