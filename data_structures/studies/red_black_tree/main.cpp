#include <iostream>
#include <list>
#include <ctime>
#include <cmath>
#include <set>

#include "red_black_tree.h"

#define PRINT_MAX 10

/*
  Zasady:
    1. każdy węzeł ma kolor (czerwony lub czarny),
    2. korzeń jest czarny,
    3. każdy liść jest czarny (przy czym jako liście nie są traktowane najniższe węzły, zaś puste wskaźniki
    potomne odchodzące od nich — tzw. NULLe lub NILe),
    4. czerwony węzeł musi mieć czarne dzieci,
    5. każda ścieżka z dowolnego ustalonego węzła do dowolnego osiągalnego liścia ma tyle samo czarnych
    węzłów
 */

void printTreeInfo(RedBlackTree<int> &bst) {
    std::cout << "Size: " << bst.size() << std::endl;
    std::cout << "Height: " << bst.height(*bst.root()) << std::endl;
    std::list<std::string> strs = bst.nodeStr(*bst.root());
    int i = 0;
    for (auto j = strs.begin(); j != strs.end() && i < PRINT_MAX; j++, i++) {
        std::cout << *j;
    }
    if(i < strs.size()-2) {
        std::cout << "..." << std::endl;
        std::cout << strs.back();
    }
}

int kRand(int min, int max)
{
    return (rand() % (max-min) + min);
}

int main(int argc, char const *argv[])
{
    srand (time(nullptr));
    RedBlackTree<int> tree;
    const int MAX_ORDER = 7;
    const int MIN = 0;
    const int MAX = pow(10, 7);

    for (int o = 1; o <= MAX_ORDER; ++o) {
        const int n = pow(10, o);

        std::set<int> values;
        clock_t t1 = clock();
        for (int i = 0; i < n; ++i) {
            int tmp;
            do {
                tmp =  kRand(MIN, MAX);
            } while(values.find(tmp) != values.end());
            values.insert(tmp);
            tree.insert(tmp, tree.root());
        }
        clock_t t2 = clock();

        printTreeInfo(tree);
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;

        const int m = pow (10 , 4);
        int hits = 0;
        t1 = clock();
        for (int i = 0; i < m; ++i) {
            int tmp =  kRand(MIN, MAX);
            if(tree.find(tmp, *tree.root()) != nullptr)
                hits++;
        }
        t2 = clock();
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        std::cout << "Hits: " << hits << std::endl;
        tree.clear();
    }
    return 0;
}