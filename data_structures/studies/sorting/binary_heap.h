#ifndef LAB8_BINARY_HEAP_DA_H
#define LAB8_BINARY_HEAP_DA_H

#include <string>
#include <vector>

const size_t START_SIZE = 100;

//binary heap dynamic array implementation
template<class T>
class BinaryHeapDA {
std::vector<T> dynamicArray;
bool (*comparator)(const T&, const T&);

static bool defaultComparator(const T&l, const T&r) {
    return l < r;
}

public:
    explicit BinaryHeapDA(size_t reserve=START_SIZE, bool (*binaryHeapComparator)(const T&, const T&)= &defaultComparator) {
        dynamicArray.reserve(reserve);
        comparator = binaryHeapComparator;
    }

    explicit BinaryHeapDA(std::vector<T> arr, bool choseToRepairBottomUp=true, bool (*binaryHeapComparator)(const T&, const T&)= &defaultComparator) {
        dynamicArray = std::move(arr);
        comparator = binaryHeapComparator;
        if(choseToRepairBottomUp)
            repairBottomUp(0);
        else
            repairTopDown(0);
    }

    void insert(T val) {
        dynamicArray.push_back(val);
        heapifyUp(dynamicArray.size()-1);
    }

    T pull() {
        if(dynamicArray.size() > 1) {
            T val = dynamicArray[0];
            dynamicArray[0] = dynamicArray[dynamicArray.size()-1];
            dynamicArray.pop_back();
            heapifyDown(0);
            return val;
        } else if(dynamicArray.size() == 1) {
            T val = dynamicArray[0];
            dynamicArray.pop_back();
            return val;
        }
        throw std::string("No elements in Binary Heap\n");
    }

    void clear() {
        dynamicArray.clear();
    }

    std::vector<T> sort() {
        std::vector<T> values;
        while(dynamicArray.size() != 0)
            values.insert(values.begin(), pull());
        return values;
    }

    std::vector<std::string> to_string(size_t firstFew = 100) {
        int n = dynamicArray.size();
        std::vector<std::string> response;
        for (int i = 0; i < n && i < firstFew; ++i) {
            response.push_back(std::to_string(i) + ": val="+std::to_string(dynamicArray[i]) + ", children=[");
            if(2*i + 1 < n)
                response[i] += std::to_string(dynamicArray[2*i+1]);
            if(2*i + 2 < n)
                response[i] += ", " + std::to_string(dynamicArray[2*i+2]);
            response[i] += "]\n";
        }

        if(firstFew < dynamicArray.size())
            response.push_back("...\n");
        return response;
    }

protected:
    void repairBottomUp(size_t index) {
        if(index <= dynamicArray.size()-1) {
            if(2*index+2 <= dynamicArray.size()-1) {
                repairBottomUp(2*index+1);
                repairBottomUp(2*index+2);
            } else if(2*index+1 <= dynamicArray.size()-1) {
                repairBottomUp(2*index+1);
            } else {
                heapifyUp(index);
            }
        }
    }

    void repairTopDown(size_t index) {
        if(index < dynamicArray.size()-1) {
            size_t childIndex = 2 * index + 1;
            if(2*index+2 <= dynamicArray.size()-1) {
                if (comparator(dynamicArray[childIndex], dynamicArray[2 * index + 2]))
                    childIndex = 2 * index + 2;
                if (comparator(dynamicArray[index], dynamicArray[childIndex])) {
                    T tmp = dynamicArray[index];
                    dynamicArray[index] = dynamicArray[childIndex];
                    dynamicArray[childIndex] = tmp;
                }
                repairTopDown(2 * index + 1);
                repairTopDown(2 * index + 2);
            } else if(2*index+1 <= dynamicArray.size()-1) {
                if (comparator(dynamicArray[index], dynamicArray[childIndex])) {
                    T tmp = dynamicArray[index];
                    dynamicArray[index] = dynamicArray[childIndex];
                    dynamicArray[childIndex] = tmp;
                }
                repairTopDown(childIndex);
            }
        }
    }

    void heapifyUp(size_t index) {
        if(index > 0) {
            size_t parentIndex = (index-1)/2;
            if(comparator(dynamicArray[parentIndex], dynamicArray[index])) {
                T tmp = dynamicArray[parentIndex];
                dynamicArray[parentIndex] = dynamicArray[index];
                dynamicArray[index] = tmp;
                heapifyUp(parentIndex);
            }
        }
    }

    void heapifyDown(size_t index) {
        if(index < dynamicArray.size()-1) {
            size_t childIndex = 2 * index + 1;
            if(2*index+2 <= dynamicArray.size()-1) {
                if (comparator(dynamicArray[childIndex], dynamicArray[2 * index + 2]))
                    childIndex = 2 * index + 2;
                if (comparator(dynamicArray[index], dynamicArray[childIndex])) {
                    T tmp = dynamicArray[index];
                    dynamicArray[index] = dynamicArray[childIndex];
                    dynamicArray[childIndex] = tmp;
                }
                heapifyDown(childIndex);
            } else if(2*index+1 <= dynamicArray.size()-1) {
                if (comparator(dynamicArray[index], dynamicArray[childIndex])) {
                    T tmp = dynamicArray[index];
                    dynamicArray[index] = dynamicArray[childIndex];
                    dynamicArray[childIndex] = tmp;
                }
                heapifyDown(childIndex);
            }
        }
    }
};

#endif //LAB8_BINARY_HEAP_DA_H
