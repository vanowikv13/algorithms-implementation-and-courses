#include <iostream>
#include <ctime>
#include <cmath>
#include <vector>
#include <algorithm>
#include <list>

#include "binary_heap.h"

#define MIN 1
#define MAX 10000000

int kRand(int min, int max)
{
    return (rand() % (max-min) + min);
}

bool comparator(const int &f, const int &s) {
    return f < s;
}

int getBucketIndex(const int &x) {
    return x / MAX;
}

void countingSort(std::vector<int> &arr, int m = MAX) {
    std::vector<int> count(m+1, 0);
    for(auto &x : arr)
        count[x]++;

    for (int i = 1; i < count.size(); i++)
        count[i] += count[i-1];

    std::vector<int> result(arr.size());
    for(int i = result.size()-1; i>=0; i--) {
        result[count[arr[i]] - 1] = arr[i];
        count[arr[i]]--;
    }
    arr = std::move(result);
}

//operator < overload required
template<class T>
void bucketSort(std::vector<T> &arr, int bucketNums, int (*getBucketIndex)(const T&)) {
    std::vector<std::list<T>> buckets(bucketNums);
    for (int i = 0; i < arr.size(); ++i)
        buckets[getBucketIndex(arr[i]) % bucketNums].push_back(arr[i]);
    std::vector<T> endList;
    for (int i = 0; i < buckets.size(); ++i) {
        buckets[i].sort();
        endList.insert(endList.end(), buckets[i].begin(), buckets[i].end());
    }

    //TODO: repair it
    arr = std::move(endList);
}

template<class T>
void printFew(std::vector<T> &arr, int n) {
    int m = n > arr.size() ? arr.size() : n / 2;
    for (int i = 0; i < m; ++i)
        std::cout << arr[i] << " ";

    if(n < arr.size()) {
        std::cout << " ... ";
        for (int i = arr.size() - 1 - m; i < arr.size(); ++i)
            std::cout << arr[i] << " ";
    }
    std::cout << std::endl << std::endl;
}


int main() {
    srand (time(nullptr));
    const int MAX_ORDER = 7;
    for (int o = 1; o <= MAX_ORDER; ++o) {
        int n = static_cast<int>(pow(10, o));
        std::vector<int> arr1(n);
        for (int i = 0; i < n; ++i)
            arr1[i] = kRand(MIN, MAX);

        std::vector<int> arr2(arr1);
        std::vector<int> arr3(arr1);
        clock_t t1 = clock();
        countingSort(arr1);
        clock_t t2 = clock();
        std::cout << "Time for countingSort: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        printFew(arr1, 15);

        t1 = clock();
        BinaryHeapDA<int> bh(arr2, false);
        arr2 = std::move(bh.sort());
        t2 = clock();
        std::cout << "Time for heapSort: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        printFew(arr2, 15);

        t1 = clock();
        bucketSort(arr3, 10, getBucketIndex);
        t2 = clock();
        std::cout << "Time for bucketSort: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        printFew(arr3, 15);
    }
    return 0;
}
