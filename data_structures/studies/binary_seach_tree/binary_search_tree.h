#pragma once
#include <list>
#include <string>

template <class T>
class BinarySearchTree
{
    class Node
    {
    public:
        uint key;
        T value;
        Node *left = nullptr;
        Node *right = nullptr;
        Node *parent = nullptr;
        explicit Node(uint i, T val) : key(i), value(val) {}

        std::string str(std::string prev) {
            std::string i=" [";
            if(parent != nullptr)
                i += " p: " + std::to_string(parent->key)+",";
            else
                i += " p: NULL,";
            if(left != nullptr)
                i += " l: " + std::to_string(left->key)+",";
            else
                i += " l: NULL,";

            if(right != nullptr)
                i += " r: " + std::to_string(right->key)+" ";
            else
                i += " r: NULL ";

            i+= "]";
            return ("|" + prev + "> key= " + std::to_string(key) + ", value= " + std::to_string(value) + ", " + i + "\n");
        }
    };

    size_t size_ = 0;
    Node *root_ = nullptr;

public:
    BinarySearchTree() = default;

    std::list<T> preorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values{node->value};
        if (node->left != nullptr)
            values.splice(values.end(), preorder(node->left));

        if (node->right != nullptr)
            values.splice(values.end(), preorder(node->right));

        return values;
    }

    std::list<T> inorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values;
        if (node->left != nullptr)
            values.splice(values.end(), inorder(node->left));

        values.push_back(node->value);
        if (node->right != nullptr)
            values.splice(values.end(), inorder(node->right));
        return values;
    }

    void insert(uint key, T value, Node **node)
    {
        if (*node == nullptr)
        {
            *node = new Node(key, value);
            (*node)->parent = nullptr;
            size_++;
        }
        else if (value < (*node)->value)
        {
            if ((*node)->left == nullptr)
            {
                (*node)->left = new Node(key, value);
                (*node)->left->parent = (*node);
                size_++;
            }
            else
                insert(key, value, &((*node)->left));
        }
        else
        {
            if ((*node)->right == nullptr)
            {
                (*node)->right = new Node(key, value);
                (*node)->right->parent = (*node);
                size_++;
            }
            else
                insert(key, value, &((*node)->right));
        }
    }

    void remove(T value, Node **node)
    {
        if (*node == nullptr)
            return;
        else if (value < (*node)->value)
            remove(value, &((*node)->left));
        else if (value > (*node)->value)
            remove(value, &((*node)->right));
        else
        {
            Node* parent = (*node)->parent;
            if ((*node)->left == nullptr && (*node)->right == nullptr)
            {
                if((*node) == root_) {
                    delete root_;
                    root_ = nullptr;
                }
                else if (parent->left == (*node))
                {
                    delete parent->left;
                    parent->left = nullptr;
                }
                else
                {
                    delete parent->right;
                    parent->right = nullptr;
                }
                size_--;
                return;
            }

            Node *directionTmp = (*node)->left == nullptr ? (*node)->right : (*node)->left;
            if((*node)->left == nullptr || (*node)->right == nullptr)
            {
                Node *tmp = (*node);
                if(parent == nullptr)
                    root_ = directionTmp;
                else if(parent->right == (*node))
                    parent->right = directionTmp;
                else
                    parent->left = directionTmp;
                directionTmp->parent = parent;
                delete tmp;
                size_--;
            } else
            {
                Node *successor = (*node)->right;
                while (successor->left != nullptr)
                    successor = successor->left;
                (*node)->value = successor->value;
                remove(successor->value, &successor);
            }
        }
    }

    Node* find(uint key, Node*node) {
        if(node == nullptr)
            return nullptr;
        if(node->value == key)
            return node;
        if(node->value < key)
            return find(key, node->left);
        if(node->value > key)
            return find(key, node->right);
        return nullptr;
    }

    uint height(Node* node) {
        if(node == nullptr)
            return 0;
        uint left = height(node->left);
        uint right = height(node->right);
        if(left > right)
            return left + 1;
        return right + 1;
    }

    void clear() {
        while(root_ != nullptr)
            remove(root_->value, &root_);
    }

    std::list<std::string> nodeStr(Node*node, std::string prev="-") {
        if(node == nullptr || size_ == 0)
            return std::list<std::string>();

        std::list<std::string> strs{node->str(prev)};
        strs.splice(strs.end(), nodeStr(node->left, prev+"-"));
        strs.splice(strs.end(), nodeStr(node->right, prev+"-"));
        return strs;
    }

    Node **root()
    {
        return &root_;
    }

    size_t size()
    {
        return size_;
    }
};