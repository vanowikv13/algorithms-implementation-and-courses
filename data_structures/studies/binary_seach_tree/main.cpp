#include <iostream>
#include <algorithm>
#include <ctime>
#include <math.h>
#include <set>

#include "binary_search_tree.h"

#define PRINT_MAX 10

void printTreeInfo(BinarySearchTree<int> &bst) {
    std::cout << "Size: " << bst.size() << std::endl;
    std::cout << "Height: " << bst.height(*bst.root()) << std::endl;
    std::list<std::string> strs = bst.nodeStr(*bst.root());
    int i = 0;
    for (auto j = strs.begin(); j != strs.end() && i < PRINT_MAX; j++, i++) {
        std::cout << *j;
    }
    if(i < strs.size()-2) {
        std::cout << "..." << std::endl;
        std::cout << strs.back();
    }
}

int kRand(int min, int max)
{
    return (rand() % (max-min) + min);
}

int main(int argc, char const *argv[])
{
    srand (time(nullptr));
    BinarySearchTree<int> bst;
    const int MAX_ORDER = 7;
    const int MIN = 0;
    const int MAX = pow(10, 7);

    for (int o = 1; o <= MAX_ORDER; ++o) {
        const int n = pow(10, o);

        std::set<int> values;
        clock_t t1 = clock();
        for (int i = 0; i < n; ++i) {
            int tmp;
            do {
                tmp =  kRand(MIN, MAX);
            } while(values.find(tmp) != values.end());
            values.insert(tmp);
            bst.insert(i, tmp, bst.root());
        }
        clock_t t2 = clock();

        printTreeInfo(bst);
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;

        const int m = pow (10 , 4);
        int hits = 0;
        t1 = clock();
        for (int i = 0; i < m; ++i) {
            int tmp =  kRand(MIN, MAX);
            if(bst.find(tmp, *bst.root()) != nullptr)
                hits++;
        }
        t2 = clock();
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        std::cout << "Hits: " << hits << std::endl;
        bst.clear();
    }

    return 0;
}