#pragma once
#include <iostream>
#include <string>
#include <stdexcept>

template <class T>
class DynamicArray
{
protected:
    size_t capacity = 0;
    size_t arr_size = 0;
    T *array = nullptr;

    void allocate(size_t n)
    {
        if (capacity == 0)
            this->erase();
        array = new T[n];
        capacity = n;
    }

    void allocateNewSizeWithPreviousValues()
    {
        size_t new_arr_cap = static_cast<size_t>(capacity * allocation_factor) + 1;
        T *x = array;
        array = new T[new_arr_cap];
        for (int i = 0; i < arr_size; i++)
            array[i] = x[i];

        delete[] x;
        capacity = new_arr_cap;
    }

    static bool defaultComparer(T first, T second) {
        return first > second;
    }

    void bubbleSort(bool (*compare)(T, T)) {
        for(size_t i = 0; i < arr_size-1; i++) {
            bool swap = false;
            for(size_t j = 0; j < arr_size-1-i; j++) {
                if(compare(array[j], array[j+1])) {
                    swap = true;
                    T tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
            if(swap == false)
                break;
        }
    }

public:
    //allocation factor must be larger than 1.1
    static float allocation_factor;

    DynamicArray(size_t n)
    {
        this->allocate(n);
    }

    DynamicArray() {}

    T& operator[](size_t i)
    {
        if(i >= arr_size || i < 0)
            throw std::out_of_range ("Out of range.");
        return array[i];
    }

    void add(T value)
    {
        if (arr_size >= capacity)
            this->allocateNewSizeWithPreviousValues();
        array[arr_size] = value;
        arr_size++;
    }

    T& getElement(size_t i)
    {
        if (i < arr_size && i >= 0)
            return array[i];
        throw std::out_of_range ("Out of range.");
    }

    //to print elements specify the function that will do it
    void print(void(*print_element)(T)) {
        std::cout << "array size: " << arr_size << std::endl;
        std::cout << "array capacity: " << capacity << std::endl;
        std::cout << "Address of array in memory: " << &array << std::endl;
        std::cout << "First (max 10) elements: ";
        for(size_t i = 0; i < 10 && i <arr_size; i++)
            print_element(array[i]);
        std::cout << std::endl;
    }

    size_t size()
    {
        return arr_size;
    }

    void clear()
    {
        if (capacity > 0)
        {
            delete[] array;
        }
        capacity = 0;
    }

    void sort(bool (*compare)(T, T)=nullptr)
    {
        if(compare != nullptr) {
            bubbleSort(compare);
        } else {
            bubbleSort(&defaultComparer);
        }
    }

    ~DynamicArray()
    {
        this->clear();
    }
};

template <class T>
float DynamicArray<T>::allocation_factor = 2.0f;