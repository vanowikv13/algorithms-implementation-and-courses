#include <iostream>
#include <string>
#include <time.h>
#include <math.h>

#include "dynamic_array.h"

int getRand()
{
    return (rand() % 26 + 65);
}

void print_ele(char x) {
    std::cout << x << ", ";
}

bool comparer(char x, char y) {
    return x > y;
}

int main()
{
    srand(time(NULL));
    DynamicArray<char> *da = new DynamicArray<char>();
    const size_t order = 7;
    const size_t n = pow(10, order);

/*  //Example of other functionalities
    for(int i = 0; i < 10; i++) {
        char x = static_cast<char>(getRand());
        std::cout << x << ", ";
        da->add(x);
    }
    std::cout << std::endl;

    std::cout << "size " << da->size() << std::endl;
    da->sort(&comparer); //da->sort(); //also work like this, but use operator > comparer

    for(int i = 0; i < 10; i++) {
        std::cout << (*da)[i] << ", ";
    }

    da->print(&print_ele);
    std::cout << std::endl;
    da->clear();
*/

    clock_t t1 = clock();
    double max_time_per_element = 0.0;
    for (size_t i = 0; i < n; i++)
    {
        clock_t begin = clock();
        da->add(static_cast<char>(getRand()));
        clock_t end = clock();
        double time_per_element = (double)(end - begin) / CLOCKS_PER_SEC;
        if (time_per_element > max_time_per_element)
        {
            max_time_per_element = time_per_element;
            std::cout << "Index: " << i << "\tNew adding worst time: " << time_per_element << std::endl;
        }
    }

    clock_t t2 = clock();
    double full_time = (double)(t2 - t1) / CLOCKS_PER_SEC;
    std::cout << "Full time: " << full_time << std::endl;

    da->clear();
    delete da;
    return 0;
}