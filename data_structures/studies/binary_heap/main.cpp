#include <iostream>
#include <ctime>
#include <cmath>

#include "binary_heap_da.h"

int kRand(int min, int max)
{
    return (rand() % (max-min) + min);
}

bool comparator(const int &f, const int &s) {
    return f < s;
}


int main() {
    srand (time(nullptr));
    const int MAX_ORDER = 7;
    int MIN = 0;

    for (int o = 1; o <= MAX_ORDER; ++o) {
        const int n = pow(10, o);
        int MAX = n+1;
        //instead of giving comparator in every call I just give it on construction of Binary Heap
        BinaryHeapDA<int> heap(n, &comparator);

        clock_t t1 = clock();
        for (int i = 0; i < n; ++i)
            heap.insert(kRand(MIN, MAX));
        clock_t t2 = clock();

        auto vecStr = heap.to_string();
        for (auto &s : vecStr) {
            std::cout << s;
        }
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;

        t1 = clock();
        for (int i = 0; i < n; ++i)
            heap.pull();
        t2 = clock();
        std::cout << "Time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << std::endl;
        heap.clear();
    }
    return 0;
}
