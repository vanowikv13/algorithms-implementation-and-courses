#include <iostream>
#include <ctime>
#include <fstream>
#include "hash_table.h"

void insertAndPrintErrors(HashTable &hs, int value) {
    try {
        hs.insertValue(value);
    } catch(std::string &err) {
        std::cout << err;
    }
}

void deleteAndPrintErrors(HashTable &hs, int value) {
    try {
        hs.deleteValue(value);
    } catch(std::string &err) {
        std::cout << err;
    }
}

int main() {
    srand (time(NULL));
    int X, k1, k2, k3, k4;

    std::fstream file;
    file.open("inlab07.txt", std::ios::in);
    if(!file.is_open()) {
        std::cout << "File is not specified" << std::endl;
        return 1;
    }

    file >> X >> k1 >> k2 >> k3 >> k4;

    file.close();
    HashTable hs;
    clock_t t1 = clock();
    hs.initializeArray();
    hs.setLinearAddressing();
    deleteAndPrintErrors(hs, k1);
    hs.insertValue(k1);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    hs.insertMultipleRandomKeys(X);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    insertAndPrintErrors(hs, k2);
    insertAndPrintErrors(hs, k3);
    insertAndPrintErrors(hs, k4);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    std::cout << hs.getPrintableTableInRange(500, 600) << std::endl;
    deleteAndPrintErrors(hs, k3);
    deleteAndPrintErrors(hs, k4);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    std::cout << hs.getPrintableTableInRange(500, 600) << std::endl;
    clock_t t2 = clock();
    double full_time = (double)(t2 - t1) / CLOCKS_PER_SEC;
    std::cout << "Full time: " << full_time << std::endl;

    hs.initializeArray();
    t1 = clock();
    hs.initializeArray();
    hs.setClassicDoubleMixing();
    deleteAndPrintErrors(hs, k1);
    hs.insertValue(k1);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    hs.insertMultipleRandomKeys(X);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    insertAndPrintErrors(hs, k2);
    insertAndPrintErrors(hs, k3);
    insertAndPrintErrors(hs, k4);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    std::cout << hs.getPrintableTableInRange(500, 600) << std::endl;
    deleteAndPrintErrors(hs, k3);
    deleteAndPrintErrors(hs, k4);
    std::cout << hs.getPrintableTableInRange(0, 100) << std::endl;
    std::cout << hs.getPrintableTableInRange(500, 600) << std::endl;
    t2 = clock();
    full_time = (double)(t2 - t1) / CLOCKS_PER_SEC;
    std::cout << "Full time: " << full_time << std::endl;
    return 0;
}
