#pragma once
#include <cmath>
#include <vector>
#include <string>
#include <ctime>
#include <set>

class HashTable {
    enum class Options {
        LINEAR_ADDRESSING, CLASSIC_DOUBLE_MIXING
    } option = Options::LINEAR_ADDRESSING;

    std::vector<int> table;
    static const int maxSize = 997;
    int _size = 0;
public:
    void setLinearAddressing() {
        option = Options::LINEAR_ADDRESSING;
    }

    void setClassicDoubleMixing() {
        option = Options::CLASSIC_DOUBLE_MIXING;
    }

    HashTable() {
        table.resize(maxSize);
    }

    void initializeArray(int size=maxSize) {
        table.clear();
        _size = 0;
        table.resize(size);
    }

    static int hashFunction(int key) {
        return ((key % 1000) + static_cast<int>(pow(2, key % 10)) + 1) % maxSize;
    }

    //used only by CLASSIC_DOUBLE_MIXING
    static int secondHashFunction(int key) {
        return (3*key) % 19 + 1;
    }

    void deleteValue(int value) {
        int key = find(value);
        if(key == -1 || key == 0)
            throw std::string("No such element, with value: " + std::to_string(value) + ".\n");
        table[key] = -1;
        _size--;
    }

    void insertValue(int value) {
        if(_size >= maxSize)
            throw std::string("Table is full. Cannot insert an element.\n");
        table[findFreeKey(value)] = value;
        _size++;
    }

    void insertMultipleRandomKeys(int amount, int leftRange=20000, int rightRange=40000) {
        std::set<int> values;
        for (int i = 0; i < amount && (maxSize-_size) > 0; ++i) {
            int tmp = (rand() % (rightRange-leftRange) + leftRange);
            while(values.find(tmp) != values.end())
                tmp = (rand() % (rightRange-leftRange) + leftRange);
            values.insert(tmp);
            insertValue(tmp);
        }
    }

    int find(int value) {
        int key = findKey(value);
        if(key == -1)
            throw std::string("No such element, with value: " + std::to_string(value) + ".\n");
        return key;
    }

    std::string getPrintableTableInRange(int leftRange=0, int rightRange=maxSize) {
        if(leftRange < 0 || rightRange > maxSize)
            throw std::string("Wrong range of table.\n");
        std::string str = "Size: " + std::to_string(_size) + "\n";
        for (int i = leftRange; i < rightRange; ++i)
            str += "[" + std::to_string(i) + "] = " + std::to_string(table[i]) + "\n";
        return str;
    }

protected:
    int findFreeKey(int value) {
        int key = hashFunction(value);
        int i = 1;
        if(option == Options::LINEAR_ADDRESSING) {
            while(table[key] != 0 && table[key] != -1 && i < maxSize) {
                key = hashFunction(key);
                i++;
            }
        } else if(option == Options::CLASSIC_DOUBLE_MIXING) {
            while(table[key] != 0 && table[key] != -1 && i < maxSize) {
                key = secondHashFunction(key);
                i++;
            }
        } else
            throw std::string("No option is set");
        if(table[key] == 0 || table[key] == -1)
            return key;
    }

    int findKey(int value) {
        if(_size >= maxSize || _size == 0)
            return -1;

        int key = hashFunction(value);
        int i = 1;
        if(option == Options::LINEAR_ADDRESSING) {
            while(table[key] != value && i < maxSize) {
                key = hashFunction(key);
                i++;
            }
        } else if(option == Options::CLASSIC_DOUBLE_MIXING) {
            while(table[key] != value && i < maxSize) {
                key = secondHashFunction(key);
                i++;
            }
        } else
            throw std::string("No option is set");

        if(table[key] == value)
            return key;
        return -1;
    }
};