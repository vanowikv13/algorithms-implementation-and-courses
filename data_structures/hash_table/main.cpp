#include <iostream>
#include <string>

#include "hash_table.h"

int main() {
    HashTable<int> hashTable(10);
    for (size_t i = 0; i < 15; i++)
        hashTable.insertNode(std::to_string(static_cast<int>(i)), i);
    for(size_t i = 14; i >= 0 && i <= 15; i--)
        std::cout << *hashTable.lookup(std::to_string(static_cast<int>(i))) << ", ";
    std::cout << std::endl;

    std::cout << "Size: " << hashTable.size() << std::endl;
    for (size_t i = 0; i < 15; i++)
        hashTable.deleteNode(std::to_string(static_cast<int>(i)));
    std::cout << "Size: " << hashTable.size() << std::endl;
    
}
