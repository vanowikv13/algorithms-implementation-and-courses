#pragma once
#include <stdexcept>
#include <string>

template<class T>
class HashTable {
protected:
    struct Node {
        std::string key;
        T value;
        Node* next;

        Node(std::string k, T val, Node* n = nullptr) : key(k), value(val), next(n) {}
    };

    int hashFunction(std::string key) {
        int hash = 5381;
        for (size_t i = 0; i < key.size(); i++)
        {
            hash = ((hash*33) ^ static_cast<int>(key[i])) % 0x10000000;
        }
        return hash;
    }

    size_t _size, _capacity;
    Node** table;
public:
    HashTable(size_t capacity) : _capacity(capacity) {
        _size = 0;
        table = new Node*[capacity];
        for (size_t i = 0; i < capacity; i++)
            table[i] = nullptr;
        
    }

    //best case: O(1), worst case: O(n)
    T* lookup(std::string key) {
        int index = hashFunction(key) % _capacity;
        Node* existing_node = table[index];

        if(existing_node != nullptr)
            for(Node* it = existing_node; it != nullptr; it = it->next)
                if(it->key == key)
                    return &(it->value);
        return nullptr;
    }

    //best case: O(1), worst case: O(n)
    void deleteNode(std::string key) {
        int index = hashFunction(key) % _capacity;
        Node* existing_node = table[index];
        
        if(existing_node != nullptr) {
            Node* last_node = nullptr;
            for(Node* it = existing_node; it != nullptr; it = it->next) {
                if(it->key == key) {
                    if(last_node != nullptr) {
                        last_node->next = it->next;
                        delete it;
                    } else {
                        if(existing_node->next == nullptr) {
                            delete existing_node;
                            table[index] = nullptr;
                        } else {
                            table[index] = existing_node->next;
                            delete existing_node;
                        }
                    }
                    _size--;
                    return;
                }
                last_node = it;
            }
        }
    }

    //best case: O(1), worst case: O(n)
    void insertNode(std::string key, T value) {
        int index = hashFunction(key) % _capacity;

        Node* new_node = new Node(key, value);
        Node* existing_node = table[index];
        
        if(existing_node != nullptr) {
            Node* last_node = nullptr;
            for(Node* it = existing_node; it != nullptr; it = it->next) {
                if(it->key == key) {
                    it->value = value;
                    return;
                }
                last_node = it;
            }
            last_node->next = new_node;
            
        }
        else
            table[index] = new_node;
        _size++;
    }

    size_t size() const {
        return _size;
    }
};