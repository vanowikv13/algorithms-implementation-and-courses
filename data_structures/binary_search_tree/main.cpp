#include <iostream>
#include <list>
#include <random>
#include <algorithm>
#include <vector>

#include "binary_search_tree.h"

int main(int argc, char const *argv[])
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(1,20);
    BinarySearchTree<int> bst;
    for (int i = 0; i < 1000; i++)
        bst.insert(dist6(rng), bst.root());

    std::list<int> tl = bst.bfs(*bst.root());
    std::vector<int> x(tl.begin(), tl.end());
    for(auto &xi : x)
        std::cout << xi << std::endl;

    std::cout << "Size: " << bst.size() << std::endl;
    std::shuffle(x.begin(), x.end(), std::mt19937(std::random_device()()));
    for(auto & xi : x)
      bst.remove(xi, bst.root());
    std::cout << "Size: " << bst.size() << std::endl;
    return 0;
}