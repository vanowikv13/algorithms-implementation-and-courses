#pragma once
#include <list>

template <class T>
class BinarySearchTree
{
    class Node
    {
    public:
        T value;
        Node *left = nullptr;
        Node *right = nullptr;
        explicit Node(T val) : value(val) {}
    };

    size_t size_ = 0;
    Node *root_ = nullptr;

public:
    BinarySearchTree() = default;

    std::list<T> preorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values{node->value};
        if (node->left != nullptr)
            values.splice(values.end(), preorder(node->left));

        if (node->right != nullptr)
            values.splice(values.end(), preorder(node->right));

        return values;
    }

    std::list<T> postorder(Node *node)
    {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values;
        if (node->left != nullptr)
            values.splice(values.end(), postorder(node->left));

        if (node->right != nullptr)
            values.splice(values.end(), postorder(node->right));

        values.push_back(node->value);
        return values;
    }

    std::list<T> bfs(Node *node, bool firstCall=true) {
        if (node == nullptr)
            return std::list<T>();

        std::list<T> values;
        if(firstCall)
            values.push_back(node->value);

        if (node->left != nullptr) {
            values.push_back(node->left->value);
            values.splice(values.end(), bfs(node->left, false));
        }

        if (node->right != nullptr) {
            values.push_back(node->right->value);
            values.splice(values.end(), bfs(node->right, false));
        }
        return values;
    }

    void insert(T value, Node **node)
    {
        if (*node == nullptr)
        {
            *node = new Node(value);
            size_++;
        }
        else if (value < (*node)->value)
        {
            if ((*node)->left == nullptr)
            {
                (*node)->left = new Node(value);
                size_++;
            }
            else
                insert(value, &((*node)->left));
        }
        else
        {
            if ((*node)->right == nullptr)
            {
                (*node)->right = new Node(value);
                size_++;
            }
            else
                insert(value, &((*node)->right));
        }
    }

    void remove(T value, Node **node, Node **parent = nullptr)
    {
        if (*node == nullptr)
            return;
        else if (value < (*node)->value)
            remove(value, &((*node)->left), node);
        else if (value > (*node)->value)
            remove(value, &((*node)->right), node);
        else
        {
            if ((*node)->left == nullptr && (*node)->right == nullptr)
            {
                if((*node) == root_) {
                    delete root_;
                    root_ = nullptr;
                }
                else if ((*parent)->left == (*node))
                {
                    delete (*parent)->left;
                    (*parent)->left = nullptr;
                }
                else
                {
                    delete (*parent)->right;
                    (*parent)->right = nullptr;
                }
                size_--;
                return;
            }

            Node *directionTmp = (*node)->left == nullptr ? (*node)->right : (*node)->left;
            if((*node)->left == nullptr || (*node)->right == nullptr)
            {
                Node *tmp = (*node);
                if(parent == nullptr)
                    root_ = directionTmp;
                else if((*parent)->right == (*node))
                    (*parent)->right = directionTmp;
                else
                    (*parent)->left = directionTmp;
                delete tmp;
                size_--;
            } else
            {
                Node *successor = (*node)->right;
                Node *successor_parent = (*node);
                while (successor->left != nullptr)
                {
                    successor_parent = successor;
                        successor = successor->left;
                }
                (*node)->value = successor->value;
                remove(successor->value, &successor, &successor_parent);
            }
        }
    }

    Node **root()
    {
        return &root_;
    }

    size_t size()
    {
        return size_;
    }
};