#pragma once
#include <stdexcept>
#include <string>

/*
    Double ended queue simpler called deque is just queue with options to
    remove element from tail and add element to head. Here we will be used
    double linked list implementation. Deque could also be build on linked list
    but in this simple implementation it would only improve data ussage
    and lower performance of pop_back() from O(1) to O(n).
*/

template<class T>
class Deque {
protected:
    struct Node {
        T value;
        Node* next, *prev;

        Node(T val, Node* p = nullptr, Node* n = nullptr) : value(val), next(n), prev(p) {}
    };

    Node* head = nullptr, *tail = nullptr;
    size_t _size=0;

public:
    //O(1)
    void push_front(T value) {
        Node* tmp = new Node(value);
        if(_size == 0) {
            head = tmp;
            tail = tmp;
        } else {
            tmp->next = head;
            head->prev = tmp;
            head = tmp;
        }
        _size++;
    }

    //O(1)
    void push_back(T value) {
        Node* tmp = new Node(value);
        if(_size == 0) {
            head = tmp;
            tail = tmp;
        } else {
            tmp->next = nullptr;
            tmp->prev = tail;
            tail->next = tmp;
            tail = tmp;
        }
        _size++;
    }

    //O(1)
    T pop_front() {
        if(_size == 0)
            throw std::string("Deque is empty. Could not pop_front.");
        
        T ret = head->value;
        if(_size == 1) {
            tail = nullptr;
            delete head;
            head = nullptr;
        } else {
            Node* tmp = head->next;
            delete head;
            tmp->prev = nullptr;
            head = tmp;
        }
        _size--;
        return ret;
    }

    //O(1)
    T pop_back() {
        if(_size == 0)
            throw std::string("Deque is empty. Could not pop_back.");
        
        T ret = tail->value;
        if(_size == 1) {
            tail = nullptr;
            delete head;
            head = nullptr;
        } else {
            Node* tmp = tail->prev;
            tmp->next = nullptr;
            delete tail;
            tail = tmp;
        }
        _size--;
        return ret;
    }

    //O(1)
    T front() const {
        if(_size == 0)
            throw std::string("Deque is empty. Could not get front value.");

        return head->value;
    }

    //O(1)
    T back() const {
        if(_size == 0)
            throw std::string("Deque is empty. Could not get front value.");

        return tail->value;
    }

    size_t size() const {
        return _size;
    }
};