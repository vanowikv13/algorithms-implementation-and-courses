#include <iostream>
#include <string>

#include "deque.h"

int main() {
    Deque<int> deque;
    for(size_t i = 0; i < 3; i++) {
        deque.push_back(i+1);
        deque.push_front(10-i);
    }
    
    std::cout << deque.size() << std::endl;
    
    std::cout << deque.front() << std::endl;

    try {
        for (size_t i = 0; i < 3; i++)
        {
            std::cout << deque.pop_back() << ",";
            std::cout << deque.pop_front() << ",";
        }
        std::cout << std::endl;
        
    } catch(std::string err) {
        std::cerr << std::endl << err << std::endl;
    }
}
